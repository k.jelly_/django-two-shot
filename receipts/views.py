from django.urls import reverse_lazy
from django.shortcuts import redirect
from receipts.models import Account, ExpenseCategory, Receipt
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receipts"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        formsave = form.save(commit=False)
        formsave.purchaser = self.request.user

        form.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categoryList.html"
    context_object_name = "categories"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/categoryCreate.html"
    fields = ["name"]
    success_url = reverse_lazy("list_categories")

    def form_vaid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        form.save()

        return redirect("list_categories")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accountList.html"
    context_object_name = "accounts"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/createAccount.html"
    context_object_name = "account"
    fields = ["name", "number"]
    success_url = reverse_lazy("list_accounts")

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        form.save()

        return redirect("list_accounts")
